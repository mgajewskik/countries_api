# Countries API

ERD online available here - [ERD](https://dbdiagram.io/d/60701050ecb54e10c33f78d7)

## Setup

### Requirements

To use this API following are required:

* python3.9
* poetry

### Installation

To install packages into virtual environment use:

```bash
poetry install
```

### Database

To run postgres database:

```bash
docker-compose up -d
```

## Development

### To run dev server

```bash
poetry run python manage.py runserver
```

### Code formatting

To format code with darker:

```bash
poetry run darker
```

### Pushing

To push to all remotes Gitlab and Github:

```bash
git push all
```
