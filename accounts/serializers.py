from rest_framework import serializers

from .models import Hero


# EXAMPLE SERIALIZER
class HeroSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Hero
        fields = ("id", "name", "alias")
